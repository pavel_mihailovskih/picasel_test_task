ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'picasel_service',
        'USER': 'picasel_service',
        'PASSWORD': 'picasel_service',
        'HOST': 'picasel_postgres',
        'PORT': '5432',
    }
}
