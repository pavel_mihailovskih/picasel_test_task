import secrets

from django.db import models


class TokenQuerySet(models.QuerySet):

    def get_new_token_for_user(self, user):
        token = secrets.token_hex(20)
        return self.create(token=token, user=user)


class TokenManager(models.Manager):
    pass
