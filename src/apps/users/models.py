from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.users.managers import TokenManager, TokenQuerySet


class UserManager(BaseUserManager):
    """
    Define a model manager for User model with no username field.
    """
    use_in_migrations = True

    def _create_user(self, email, password, first_name, last_name, patronymic, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          first_name=first_name,
                          last_name=last_name,
                          patronymic=patronymic,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name, last_name, patronymic, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, first_name, last_name, patronymic, **extra_fields)


class User(AbstractUser):
    """
    Модель пользователя
    """
    username = None
    first_name = models.CharField(_('first name'), max_length=30, db_index=True)
    last_name = models.CharField(_('last name'), max_length=150, db_index=True)
    patronymic = models.CharField(_('Отчество'), max_length=30, db_index=True)
    email = models.EmailField(_('Email адрес'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'patronymic']

    objects = UserManager()

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name, self.patronymic]).strip()


class Token(models.Model):
    """
    Модель токенов для назначения/снятия задачи с польователя.
    Токен действует определенное время. Время действия указвывется в настройках.
    """
    user = models.ForeignKey(User, related_name='tokens', on_delete=models.CASCADE)
    token = models.CharField(_("Токен"), max_length=40, unique=True, db_index=True)
    created = models.DateTimeField(_("Время создания"), auto_now_add=True)

    objects = TokenManager.from_queryset(TokenQuerySet)()
