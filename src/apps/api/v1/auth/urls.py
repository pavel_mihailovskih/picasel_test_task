from django.conf.urls import url

from rest_auth.views import (
    LogoutView
)
from rest_framework.authtoken.views import ObtainAuthToken

urlpatterns = [
    url(r'^login/$', ObtainAuthToken.as_view(), name='rest_login'),
    # URLs that require a user to be logged in with a valid session / token.
    url(r'^logout/$', LogoutView.as_view(), name='rest_logout'),
]
