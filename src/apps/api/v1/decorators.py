from django.utils.translation import ugettext_lazy as _
from rest_framework.validators import ValidationError


def required_any_query_params(query_params: set):
    """
    Декоратор проверки обязательных параметров в GET запросе
    :param query_params: параметры, тип множество
    :return:
    """
    def decorator(fn):
        def decorated(*args, **kwargs):
            request = args[0]
            request_query_params = set(request.query_params.keys())
            required_param_in_request = query_params.intersection(request_query_params)
            if not required_param_in_request:
                raise ValidationError(
                    _("В запросе должен присутствовать один "
                      "из обязательных параметров {query_params}".format(query_params=query_params)))

            return fn(*args, **kwargs)

        return decorated

    return decorator
