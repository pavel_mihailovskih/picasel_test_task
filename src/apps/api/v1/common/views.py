from rest_framework.settings import api_settings


class SerializerClassMixin:
    """
    Выбор класса сериализатора в зависимости от action (типа запроса)
    """
    serializer_class_map = None

    def get_serializer_class_map(self):
        assert self.serializer_class_map is not None, (
                "'%s' should either include a `serializer_class_map` attribute."
                % self.__class__.__name__
        )
        return self.serializer_class_map

    def get_serializer_class(self):
        serializer_class_map = self.get_serializer_class_map()
        return serializer_class_map[self.action]


class PermissionClassesMixin:
    """
    Выбор класса проверки permissions в зависимости от action (типа запроса)
    """
    permission_classes_map = None

    def get_permission_classes_map(self):
        assert self.permission_classes_map is not None, (
                "'%s' should either include a `permission_classes_map` attribute."
                % self.__class__.__name__
        )
        return self.permission_classes_map

    def get_permissions(self):
        permission_classes_map = self.get_permission_classes_map()
        self.permission_classes = permission_classes_map.get(self.action, api_settings.DEFAULT_PERMISSION_CLASSES)
        return super().get_permissions()


class ThrottleClassesMixin:
    """
    Выбор класса Throttle (ограничение количества запросов) в зависимости от action (типа запроса)
    """
    throttle_classes_map = None

    def get_throttle_classes_map(self):
        assert self.throttle_classes_map is not None, (
                "'%s' should either include a `permission_classes_map` attribute."
                % self.__class__.__name__
        )
        return self.throttle_classes_map

    def get_throttles(self):
        throttle_classes_map = self.get_throttle_classes_map()
        self.throttle_classes = throttle_classes_map.get(self.action, api_settings.DEFAULT_THROTTLE_CLASSES)
        return super().get_throttles()
