from django.urls import path, include

urlpatterns = [
    path('', include('apps.api.v1.users.urls')),
    path('tasks/', include('apps.api.v1.tasks.urls')),
    path('auth/', include('apps.api.v1.auth.urls'))
]
