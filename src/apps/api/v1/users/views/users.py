from django.db import models
from django.db.models.functions import Concat
from django.utils.decorators import method_decorator
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from apps.api.v1.common.views import PermissionClassesMixin, ThrottleClassesMixin, SerializerClassMixin
from apps.api.v1.decorators import required_any_query_params
from apps.api.v1.users.permissions import IsValidToken
from apps.tasks.models import Task
from apps.users.models import User
from ..filtersets import UserFilterSet
from ..serializers import UserSerializer, AssingTaskSerializer, UserUpdateSerializer, UserCreateSerializer
from ..throttles import ChangeUserRateThrottle, DeleteUserRateThrottle


class UserViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  SerializerClassMixin,
                  PermissionClassesMixin,
                  ThrottleClassesMixin,
                  GenericViewSet):
    """Апи для фильтрации, поиска, создания, обновления и удаления пользователя"""
    queryset = User.objects.annotate(
        full_name=Concat(
            models.F('first_name'), models.Value(' '), models.F('last_name'), models.Value(' '), models.F('patronymic'),
            output_field=models.CharField()
        )
    ).all()
    serializer_class_map = {
        'list': UserSerializer,
        'create': UserCreateSerializer,
        'update': UserUpdateSerializer,
        'partial_update': UserUpdateSerializer,
    }
    filter_backends = (DjangoFilterBackend,)
    filterset_class = UserFilterSet
    permission_classes_map = {
        'list': (IsAuthenticatedOrReadOnly,),
        'create': (IsAuthenticated,),
        'update': (IsAuthenticated,),
        'partial_update': (IsAuthenticated,),
        'destroy': (IsAuthenticated,),

    }
    throttle_classes_map = {
        'update': (ChangeUserRateThrottle,),
        'partial_update': (ChangeUserRateThrottle,),
        'destroy': (DeleteUserRateThrottle,),
    }

    def filter_queryset(self, queryset):
        if self.action is not 'list':
            return queryset
        return super(UserViewSet, self).filter_queryset(queryset)

    @method_decorator(
        required_any_query_params(query_params={'assigned_task', 'first_name', 'last_name', 'patronymic', 'search'})
    )
    def list(self, request, *args, **kwargs):
        return super(UserViewSet, self).list(request, *args, **kwargs)


class AssignRemoveTaskMixin:
    permission_classes = (IsValidToken,)
    qs_users = User.objects.all()
    qs_tasks = Task.objects.all()


class AssignTaskApi(AssignRemoveTaskMixin, APIView):
    """назначение на пользователя задачи"""
    serializer_class = AssingTaskSerializer

    def post(self, *args, **kwargs):
        user = get_object_or_404(self.qs_users, id=kwargs.get('user_id'))
        task = get_object_or_404(self.qs_tasks, id=kwargs.get('task_id'))
        task.users.add(user)
        serializer = self.serializer_class({'user': user, 'task': task})
        return Response(serializer.data)


class RemoveTaskApi(AssignRemoveTaskMixin, APIView):
    """Снятие с пользователя задачи"""
    def delete(self, *args, **kwargs):
        user = get_object_or_404(self.qs_users, id=kwargs.get('user_id'))
        task = get_object_or_404(self.qs_tasks, id=kwargs.get('task_id'))
        task.users.remove(user)
        return Response(status=status.HTTP_204_NO_CONTENT)
