from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from apps.users.models import Token
from ..serializers.tokens import TokenSerializer


class TokenViewSet(mixins.CreateModelMixin, GenericViewSet):
    """
    Апи получения токена для назначения/снятии задачи с пользователя
    """
    queryset = Token.objects.none()
    permission_classes = (IsAuthenticated,)
    serializer_class = TokenSerializer
