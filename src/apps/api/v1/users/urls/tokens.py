from rest_framework import routers

from apps.api.v1.users.views import (TokenViewSet)

router = routers.SimpleRouter()
router.register(r'', TokenViewSet)

urlpatterns = router.urls
