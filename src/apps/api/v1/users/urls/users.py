from django.urls import path
from rest_framework import routers

from apps.api.v1.users.views import (UserViewSet, AssignTaskApi, RemoveTaskApi)

router = routers.SimpleRouter()
router.register(r'', UserViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('<int:user_id>/task/<int:task_id>/assign/', AssignTaskApi.as_view()),
    path('<int:user_id>/task/<int:task_id>/remove/', RemoveTaskApi.as_view()),
]
