from django.utils import timezone
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import BasePermission

from apps.users.models import Token
from core.settings.app_settings.users import TOKEN_VALIDITY_PERIOD_MINUTES, DEFAULT_HTTP_ACTION_TOKEN_NAME


class IsValidToken(BasePermission):
    """
    Проверка токена на назначение задачи на пользователя
    """

    def has_permission(self, request, view):
        token_val = request.META.get(DEFAULT_HTTP_ACTION_TOKEN_NAME, None)

        if not token_val:
            return False

        token = get_object_or_404(Token.objects.all(), token=token_val)
        tz_now = timezone.get_current_timezone()
        diff_time = timezone.now() - timezone.localtime(token.created, timezone=tz_now)
        return bool(diff_time.seconds / 60 <= TOKEN_VALIDITY_PERIOD_MINUTES)
