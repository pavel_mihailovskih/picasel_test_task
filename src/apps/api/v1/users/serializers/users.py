from rest_framework import serializers

from apps.api.v1.tasks.serializers import TaskSerializer
from apps.users.models import User


class UserSerializer(serializers.ModelSerializer):
    """
    Показ пользователя
    """
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'patronymic', 'email']
        read_only_fields = ['id', 'first_name', 'last_name', 'patronymic', 'email']


class UserCreateSerializer(serializers.ModelSerializer):
    """
    Создание пользователя
    """
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'patronymic', 'email', 'password']
        read_only_fields = ['id']

    @property
    def data(self):
        """
        Из данных для показа исключаем пароль
        :return:
        """
        data = super().data
        del data['password']
        return data

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = super(UserCreateSerializer, self).create(validated_data)

        if password:
            instance.set_password(password)
            instance.save()

        return instance


class UserUpdateSerializer(serializers.ModelSerializer):
    """
    Обновление пользоваеля
    """
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'patronymic', 'email']
        read_only_fields = ['id', 'email']


class AssingTaskSerializer(serializers.Serializer):
    """
    Назначение задачи на пользователя
    """
    task = serializers.SerializerMethodField()

    def get_task(self, obj):
        return TaskSerializer(obj['task']).data
