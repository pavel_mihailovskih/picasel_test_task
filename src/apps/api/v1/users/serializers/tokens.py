from rest_framework import serializers

from apps.users.models import Token


class TokenSerializer(serializers.ModelSerializer):
    """
    Сериализатор создания токена для назначения/снятия задачи
    """
    class Meta:
        model = Token
        fields = ['token', 'created']
        read_only_fields = ['token', 'created']

    def create(self, validated_data):
        return self.Meta.model.objects.get_new_token_for_user(user=self.context['request'].user)
