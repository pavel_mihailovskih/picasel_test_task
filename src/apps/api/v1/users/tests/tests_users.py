import json
from unittest import mock

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from apps.tasks.models import Task
from apps.users.models import User, Token
from core.settings import USER_FIO_CHANGE_LIMIT_PER_MINUTE, USER_DELETION_LIMIT_PER_MINUTE, \
    TOKEN_VALIDITY_PERIOD_MINUTES


class TestCreateUserApi(APITestCase):
    def setUp(self):
        super(TestCreateUserApi, self).setUp()
        self.api_client = APIClient()
        self.user_create_params1 = {"first_name": "Any",
                                    "last_name": "Any",
                                    "patronymic": "Any",
                                    "email": "Any@name.pa",
                                    "password": "Any"}
        self.user_create_params2 = {"first_name": "Family",
                                    "last_name": "Name",
                                    "patronymic": "Patronymic",
                                    "email": "fam@name.pa",
                                    "password": "132"}

    def test_anon_create_user(self):
        """
        Создание ползователя анонимом
        :return: Ошибка 403
        """
        response = self.api_client.post(
            path='/api/v1/users/',
            data=self.user_create_params1,
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_auth_create_user(self):
        """
        Создание дубликата ползователя
        :return: Успех 201
        """
        user = User.objects.create(**self.user_create_params1)
        self.api_client.force_authenticate(user=user)
        post_params = {
            "path": "/api/v1/users/",
            "data": json.dumps(self.user_create_params2),
            "content_type": "application/json"
        }
        response = self.api_client.post(**post_params)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_auth_create_double_user(self):
        """
        Создание дубликата ползователя
        :return: Ошибка 400
        """
        user = User.objects.create(**self.user_create_params1)
        self.api_client.force_authenticate(user=user)
        post_params = {'path': '/api/v1/users/',
                       'data': json.dumps(self.user_create_params2),
                       'content_type': 'application/json'}
        self.api_client.post(**post_params)
        response = self.api_client.post(**post_params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_create_not_full_data(self):
        """
        Неполные данные
        :return: Ошибка 400
        """
        user = User.objects.create(**self.user_create_params1)
        self.api_client.force_authenticate(user=user)
        post_params = {'path': '/api/v1/users/',
                       'data': json.dumps({"first_name": "Family",
                                           "last_name": "Name",
                                           "patronymic": "Patronymic"}),
                       'content_type': 'application/json'}
        self.api_client.post(**post_params)
        response = self.api_client.post(**post_params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_create_empty_data(self):
        """
        Нет данных
        :return: Ошибка 400
        """
        user = User.objects.create(**self.user_create_params1)
        self.api_client.force_authenticate(user=user)
        post_params = {'path': '/api/v1/users/',
                       'content_type': 'application/json'}
        self.api_client.post(**post_params)
        response = self.api_client.post(**post_params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestUpdateUserApi(APITestCase):
    def setUp(self):
        super(TestUpdateUserApi, self).setUp()
        self.api_client = APIClient()
        self.user_params = {"first_name": "Any",
                            "last_name": "Any",
                            "patronymic": "Any",
                            "email": "Any@name.pa"}
        self.user_update_params = {"first_name": "Family",
                                   "last_name": "Name",
                                   "patronymic": "Patronymic"}
        self.user = User.objects.create(**self.user_params)
        self.url = "/api/v1/users/{}/".format(self.user.id)

    def test_anon_user_update(self):
        """
        Обновление ползователя анонимом
        :return: Ошибка 403
        """
        response = self.api_client.put(
            path=self.url,
            data=self.user_update_params,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_auth_user_update(self):
        """
        Обновление ползователя
        :return: Успех 201
        """

        self.api_client.force_authenticate(user=self.user)
        post_params = {
            "path": self.url,
            "data": json.dumps(self.user_update_params),
            "content_type": "application/json"
        }
        response = self.api_client.put(**post_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_auth_create_empty_data(self):
        """
        Нет данных
        :return: Ошибка 400
        """
        self.api_client.force_authenticate(user=self.user)
        post_params = {'path': self.url,
                       'content_type': 'application/json'}
        response = self.api_client.put(**post_params)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_user_update_limit(self):
        """
        Превышел лимит запросов в секунду
        :return: Ошибка 429
        """

        self.api_client.force_authenticate(user=self.user)
        post_params = {
            "path": self.url,
            "data": json.dumps(self.user_update_params),
            "content_type": "application/json"
        }
        for _ in range(0, USER_FIO_CHANGE_LIMIT_PER_MINUTE):
            self.api_client.put(**post_params)
        response = self.api_client.put(**post_params)
        self.assertEqual(response.status_code, status.HTTP_429_TOO_MANY_REQUESTS)


class TestDeleteUserApi(APITestCase):
    def setUp(self):
        super(TestDeleteUserApi, self).setUp()
        self.api_client = APIClient()
        self.user_params = {"first_name": "Any",
                            "last_name": "Any",
                            "patronymic": "Any",
                            "email": "Any@name.pa"}
        self.user_params_without_email = {"first_name": "Family",
                                          "last_name": "Name",
                                          "patronymic": "Patronymic"}
        self.user = User.objects.create(**self.user_params)

    def test_anon_user_delete(self):
        """
        Удаление ползователя анонимом
        :return: Ошибка 403
        """
        response = self.api_client.delete(
            path="/api/v1/users/{}/".format(self.user.id),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_auth_user_delete(self):
        """
        Удаление ползователя
        :return: Успех 204
        """
        user1 = User.objects.create(**self.user_params_without_email, email='admin@admin.ad')
        self.api_client.force_authenticate(user=self.user)
        post_params = {
            "path": "/api/v1/users/{}/".format(user1.id),
            "content_type": "application/json"
        }
        response = self.api_client.delete(**post_params)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_auth_user_delete_limit(self):
        """
        Удаление ползователей
        :return: Ошибка 429
        """
        users = []
        for i in range(1, USER_DELETION_LIMIT_PER_MINUTE + 5):
            users.append(User.objects.create(**self.user_params_without_email, email=f'admin{i}@admin.ad'))

        self.api_client.force_authenticate(user=self.user)
        for i in range(USER_DELETION_LIMIT_PER_MINUTE):
            post_params = {
                "path": "/api/v1/users/{}/".format(users[i].id),
                "content_type": "application/json"
            }
            self.api_client.delete(**post_params)

        post_params = {
            "path": "/api/v1/users/{}/".format(users[USER_DELETION_LIMIT_PER_MINUTE + 1].id),
            "content_type": "application/json"
        }
        response = self.api_client.delete(**post_params)
        self.assertEqual(response.status_code, status.HTTP_429_TOO_MANY_REQUESTS)


class TestUserTaskAssignRemoveApi(APITestCase):
    """
    Назначение/снятие задачи с пользователя
    """

    def setUp(self):
        super(TestUserTaskAssignRemoveApi, self).setUp()
        self.api_client = APIClient()
        self.email1 = 'test@test.te'
        self.email2 = 'admin@admin.ad'
        User.objects.bulk_create([
            User(**{"first_name": "Any",
                    "last_name": "Any",
                    "patronymic": "Any",
                    "email": self.email1}),
            User(**{"first_name": "Any",
                    "last_name": "Any",
                    "patronymic": "Any",
                    "email": self.email2}),

        ])

        self.task = Task.objects.create(name='task1', description='description1')

    def test_anon_assign_task(self):
        """
        Аноним назначает задачу на пользователя
        :return: Ошикбка 401
        """
        user1 = User.objects.get(email=self.email1)
        response = self.api_client.post(
            path=f"/api/v1/users/{user1.id}/task/{self.task.id}/assign/",
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_assign_task_without_token(self):
        """
        Пользователь без токена назначает задачу
        :return: Ошибка 403
        """
        user1 = User.objects.get(email=self.email1)
        self.api_client.force_authenticate(user=user1)
        response = self.api_client.post(
            path=f"/api/v1/users/{user1.id}/task/{self.task.id}/assign/",
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_assign_task_with_token(self):
        """
        Авторизованный пользователь с токеном назначает задачу
        :return: Успех 200
        """
        user1 = User.objects.get(email=self.email1)
        self.api_client.force_authenticate(user=user1)
        token = Token.objects.get_new_token_for_user(user=user1)
        self.api_client.credentials(HTTP_ACTION_TOKEN=token.token)
        response = self.api_client.post(
            path=f"/api/v1/users/{user1.id}/task/{self.task.id}/assign/",
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_assign_task_not_exist_task(self):
        """
        Авторизованный пользователь с токеном назначает несуществующую задачу
        :return: Успех 404
        :return:
        """
        user1 = User.objects.get(email=self.email1)
        self.api_client.force_authenticate(user=user1)
        token = Token.objects.get_new_token_for_user(user=user1)
        self.api_client.credentials(HTTP_ACTION_TOKEN=token.token)
        response = self.api_client.post(
            path=f"/api/v1/users/{user1.id}/task/{10000}/assign/",
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_user_assign_task_token_timeout(self):
        """
        Авторизованный пользователь с просроченным токеном назначает задачу
        :return: Ошибка 403
        :return:
        """
        user1 = User.objects.get(email=self.email1)
        self.api_client.force_authenticate(user=user1)
        token = Token.objects.get_new_token_for_user(user=user1)
        self.api_client.credentials(HTTP_ACTION_TOKEN=token.token)
        timeout_now = timezone.now() + timezone.timedelta(minutes=TOKEN_VALIDITY_PERIOD_MINUTES*2)
        with mock.patch.object(timezone, 'now', return_value=timeout_now):
            response = self.api_client.post(
                path=f"/api/v1/users/{user1.id}/task/{self.task.id}/assign/",
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
