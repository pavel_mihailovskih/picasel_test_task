from rest_framework.throttling import UserRateThrottle


class ChangeUserRateThrottle(UserRateThrottle):
    """проверка лимита запросов на изменение пользователя"""
    scope = 'change_user'


class DeleteUserRateThrottle(UserRateThrottle):
    """проверка лимита запросов на удаление пользователя"""
    scope = 'delete_user'
