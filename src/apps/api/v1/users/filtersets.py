import django_filters
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User


class UserFilterSet(django_filters.FilterSet):
    """
    Поиск по фио и фильтрация пользователей по фио и назначенной на них задаче
    """
    search = django_filters.CharFilter(field_name='full_name', lookup_expr='icontains', help_text=_('Поиск по ФИО'))
    first_name = django_filters.CharFilter(lookup_expr='iexact', help_text=_("Фамилия"))
    last_name = django_filters.CharFilter(lookup_expr='iexact', help_text=_("Имя"))
    patronymic = django_filters.CharFilter(lookup_expr='iexact', help_text=_("Отчество"))
    assigned_task = django_filters.NumberFilter(
        help_text=_('Назначена задача. '
                    'Например: `assigned_task=3`'),
        method='filter_assigned_task',
        label=_('Assigned task')
    )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'patronymic']

    def filter_assigned_task(self, qs, name, value):
        return qs.filter(task__id=value).distinct()
