from rest_framework.throttling import UserRateThrottle


class ChangeTaskRateThrottle(UserRateThrottle):
    """Ограничение ззапросов к API на изменение задачи"""
    scope = 'change_task'


class DeleteTaskRateThrottle(UserRateThrottle):
    """Ограничение ззапросов к API на удаление задачи"""
    scope = 'delete_task'
