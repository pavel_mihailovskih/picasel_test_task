from django.utils.decorators import method_decorator
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import GenericViewSet

from apps.api.v1.common.views import PermissionClassesMixin, ThrottleClassesMixin, SerializerClassMixin
from apps.api.v1.decorators import required_any_query_params
from apps.tasks.models import Task
from .filtersets import TaskFilterSet
from .serializers import TaskSerializer, TaskCreateSerializer, TaskUpdateSerializer
from .throttles import ChangeTaskRateThrottle, DeleteTaskRateThrottle


class TaskViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  SerializerClassMixin,
                  PermissionClassesMixin,
                  ThrottleClassesMixin,
                  GenericViewSet):
    """
    Апи для фильтрации, поиска, создания, обновления и удаления задачи
    """

    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = TaskFilterSet
    serializer_class_map = {
        'list': TaskSerializer,
        'create': TaskCreateSerializer,
        'update': TaskUpdateSerializer,
        'partial_update': TaskUpdateSerializer,
    }
    permission_classes_map = {
        'list': (IsAuthenticatedOrReadOnly,),
        'create': (IsAuthenticated,),
        'update': (IsAuthenticated,),
        'partial_update': (IsAuthenticated,),
        'destroy': (IsAuthenticated,),

    }
    throttle_classes_map = {
        'update': (ChangeTaskRateThrottle,),
        'partial_update': (ChangeTaskRateThrottle,),
        'destroy': (DeleteTaskRateThrottle,)
    }

    def filter_queryset(self, queryset):
        if self.action is not 'list':
            return queryset
        return super(TaskViewSet, self).filter_queryset(queryset)

    @method_decorator(
        required_any_query_params(query_params={'user', 'name', 'search'})
    )
    def list(self, request, *args, **kwargs):
        return super(TaskViewSet, self).list(request, *args, **kwargs)
