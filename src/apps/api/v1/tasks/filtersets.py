import django_filters
from django.utils.translation import ugettext_lazy as _

from apps.tasks.models import Task
from apps.users.models import User


class TaskFilterSet(django_filters.FilterSet):
    """
    Задачи - фильтрация по названию задачи и пользователю на которого она назначена
    """
    search = django_filters.CharFilter(field_name='name',
                                       lookup_expr='icontains',
                                       help_text=_('Поиск по названию задачи'))
    name = django_filters.CharFilter(lookup_expr='iexact', help_text=_("Название задачи"))
    user = django_filters.ModelChoiceFilter(
        help_text=_('Пользователь назначенный на задачу. '
                    'Например: `user=3`'),
        label=_('User'),
        field_name='users',
        queryset=User.objects.all()
    )

    class Meta:
        model = Task
        fields = ['name', 'user']
