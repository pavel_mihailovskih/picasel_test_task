from rest_framework import serializers

from apps.tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    """
    Сериализатор задачи для листинга
    """
    class Meta:
        model = Task
        fields = ['id', 'name', 'description', 'users']
        read_only_fields = ['id', 'name', 'description', 'users']


class TaskCreateSerializer(serializers.ModelSerializer):
    """
    Сериализатор задачи для создания задачи
    """
    class Meta:
        model = Task
        fields = ['id', 'name', 'description']


class TaskUpdateSerializer(serializers.ModelSerializer):
    """
    Сериализатор задачи для редактирования задачи
    """

    class Meta:
        model = Task
        fields = ['id', 'name', 'description']
        read_only_fields = ['id', 'description']
