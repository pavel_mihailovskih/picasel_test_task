from django.contrib.auth import get_user_model
from django.db import models


class Task(models.Model):
    """
    Модель задач
    """
    users = models.ManyToManyField(get_user_model())
    name = models.CharField(max_length=30, db_index=True)
    description = models.TextField(max_length=500, blank=True)
    completed = models.BooleanField(default=False)
