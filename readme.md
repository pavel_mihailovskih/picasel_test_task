Задание

Необходимо разработать API для управления задачами и пользователями. 

Основные возможности API:
- реализовать авторизацию (регистрацию реализовывать не нужно)
- возможность создания пользователей и задач (только для авторизированных пользователей)
- возможность удаления пользователей и задач (только для авторизированных пользователей) . 
  Пользователь может использовать действие удаления до 5 раз в минуту. 
- возможность изменения ФИО пользователей и названия задач (только для авторизированных пользователей). 
  Пользователь может использовать действие изменения до 5 раз в минуту. 

- поиск и фильтрация пользователей по ФИО
- поиск и фильтрация задач по названию
- назначение/снятие задачи на пользователя (реализовать для данного действия использование временного токена)

- получение всех задач, назначенных пользователю 
- получение всех пользователей, кому назначена задача


### Запуск проекта
1) Запуск контейнеров

    `docker-compose up -d`
2) Проверить запустились или нет контейнеры

    `docker-compose ps`
    
    Усли контейнер с БД не запустился, то запустить команду
    
    `docker-compose up -d --force-recreate picasel_postgres`
2) Загрузить дамп БД

    `cat docker/postgres/picasel_db.pgsql | docker exec -i picasel_postgres psql -U picasel_service`
    
    Пример
    
    `cat your_dump.sql | docker exec -i your-db-container psql -U postgres`
3) Доступы администратора
    
    email: admin@admin.ad
    
    pass: admin
    

### Отчет о работе API

1) Пользователи
- Авторизация пользователя

    http://joxi.ru/v29GoM1u31PNJr?d=1
    
- Создание пользователя

    http://joxi.ru/GrqGxPwuQXE8V2?d=1
    
    http://joxi.ru/8AnbOPwcj5Qn7A?d=1
    
- Редактирование ФИО пользователя 

    http://joxi.ru/4AklaPYUyv7bnr?d=1
    
    http://joxi.ru/GrqGxPwuQXEKe2?d=1
    
- Изменение ФИО не более 5 раз в минуту
    
    http://joxi.ru/1A5GvlnungRpzm?d=1
    
- Удаление пользователя

    http://joxi.ru/RmzoNPwI01nbd2?d=1

- Удаление не более 5 раз в минуту

    http://joxi.ru/E2ppXPwc9R6kD2?d=1

- поиск и фильтрация пользователей по ФИО
    
    http://joxi.ru/YmE3GQjs0NoWeA?d=1
    
    http://joxi.ru/RmzoNPwI01nL42?d=1
    
    http://joxi.ru/BA0GNkZuJLbaq2?d=1
    
    http://joxi.ru/gmvlzPwUL9EVNA?d=1

- Получение токена длня назначения задачи
    
    http://joxi.ru/Y2LQBZWU9KogpA?d=1
    
    http://joxi.ru/Dr8Pjgkt4Q6WJr?d=1

- Назначение задачи пользователю
    
    http://joxi.ru/J2bBYpGcXdROy2?d=1
    
    http://joxi.ru/xAeB69jcponxOr?d=1

- Истекло время действия токена для назначения задачи
    
    http://joxi.ru/v29GoM1u31PPor?d=1

- Снятие задачи с пользователя
    
    http://joxi.ru/l2ZxEJyUwpGWgr?d=1
    
    http://joxi.ru/nAyJoPwUYplejr?d=1

1) Задачи
- Создание задачи
    
    http://joxi.ru/RmzoNPwI01nZR2?d=1

- Редактирование названия задачи 

    http://joxi.ru/xAeB69jcponOjr

- Изменение названия задачи не более 5 раз в минуту
    
    http://joxi.ru/l2ZxEJyUwpGdwr

- Удаление задачи
    
    http://joxi.ru/EA44j8Oiwg6EwA
    
- Удаление не более 5 раз в минуту

    http://joxi.ru/82QLEnaUjLzx4m

- Получение задач назначенных пользователю
    
    http://joxi.ru/VrwzNPwiONREVA
    
- поиск и фильтрация задач по названию
    
    http://joxi.ru/KAxJNPwUMJElGm
    
    http://joxi.ru/823vE73tJVbLOm
    
    http://joxi.ru/BA0GNkZuJLb3E2
