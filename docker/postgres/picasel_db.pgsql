--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-2.pgdg90+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-2.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO picasel_service;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO picasel_service;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO picasel_service;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO picasel_service;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO picasel_service;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO picasel_service;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO picasel_service;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO picasel_service;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO picasel_service;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO picasel_service;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO picasel_service;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO picasel_service;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO picasel_service;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO picasel_service;

--
-- Name: tasks_task; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.tasks_task (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    description text NOT NULL,
    completed boolean NOT NULL
);


ALTER TABLE public.tasks_task OWNER TO picasel_service;

--
-- Name: tasks_task_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.tasks_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_task_id_seq OWNER TO picasel_service;

--
-- Name: tasks_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.tasks_task_id_seq OWNED BY public.tasks_task.id;


--
-- Name: tasks_task_users; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.tasks_task_users (
    id integer NOT NULL,
    task_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.tasks_task_users OWNER TO picasel_service;

--
-- Name: tasks_task_users_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.tasks_task_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_task_users_id_seq OWNER TO picasel_service;

--
-- Name: tasks_task_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.tasks_task_users_id_seq OWNED BY public.tasks_task_users.id;


--
-- Name: users_token; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.users_token (
    id integer NOT NULL,
    token character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_token OWNER TO picasel_service;

--
-- Name: users_token_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.users_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_token_id_seq OWNER TO picasel_service;

--
-- Name: users_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.users_token_id_seq OWNED BY public.users_token.id;


--
-- Name: users_user; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.users_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    patronymic character varying(30) NOT NULL,
    email character varying(254) NOT NULL
);


ALTER TABLE public.users_user OWNER TO picasel_service;

--
-- Name: users_user_groups; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.users_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.users_user_groups OWNER TO picasel_service;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.users_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_groups_id_seq OWNER TO picasel_service;

--
-- Name: users_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.users_user_groups_id_seq OWNED BY public.users_user_groups.id;


--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO picasel_service;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users_user.id;


--
-- Name: users_user_user_permissions; Type: TABLE; Schema: public; Owner: picasel_service
--

CREATE TABLE public.users_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.users_user_user_permissions OWNER TO picasel_service;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: picasel_service
--

CREATE SEQUENCE public.users_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_user_permissions_id_seq OWNER TO picasel_service;

--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: picasel_service
--

ALTER SEQUENCE public.users_user_user_permissions_id_seq OWNED BY public.users_user_user_permissions.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: tasks_task id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task ALTER COLUMN id SET DEFAULT nextval('public.tasks_task_id_seq'::regclass);


--
-- Name: tasks_task_users id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task_users ALTER COLUMN id SET DEFAULT nextval('public.tasks_task_users_id_seq'::regclass);


--
-- Name: users_token id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_token ALTER COLUMN id SET DEFAULT nextval('public.users_token_id_seq'::regclass);


--
-- Name: users_user id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user ALTER COLUMN id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Name: users_user_groups id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_groups ALTER COLUMN id SET DEFAULT nextval('public.users_user_groups_id_seq'::regclass);


--
-- Name: users_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.users_user_user_permissions_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add user	6	add_user
22	Can change user	6	change_user
23	Can delete user	6	delete_user
24	Can view user	6	view_user
25	Can add token	7	add_token
26	Can change token	7	change_token
27	Can delete token	7	delete_token
28	Can view token	7	view_token
29	Can add task	8	add_task
30	Can change task	8	change_task
31	Can delete task	8	delete_task
32	Can view task	8	view_task
33	Can add Token	9	add_token
34	Can change Token	9	change_token
35	Can delete Token	9	delete_token
36	Can view Token	9	view_token
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
087846093def1d6348d501e2c4d3c0f1322d7395	2019-02-21 11:16:23.735342+00	1
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	users	user
7	users	token
8	tasks	task
9	authtoken	token
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-02-21 07:29:23.680378+00
2	contenttypes	0002_remove_content_type_name	2019-02-21 07:29:23.712668+00
3	auth	0001_initial	2019-02-21 07:29:24.125423+00
4	auth	0002_alter_permission_name_max_length	2019-02-21 07:29:24.158426+00
5	auth	0003_alter_user_email_max_length	2019-02-21 07:29:24.181664+00
6	auth	0004_alter_user_username_opts	2019-02-21 07:29:24.202926+00
7	auth	0005_alter_user_last_login_null	2019-02-21 07:29:24.220783+00
8	auth	0006_require_contenttypes_0002	2019-02-21 07:29:24.236856+00
9	auth	0007_alter_validators_add_error_messages	2019-02-21 07:29:24.256657+00
10	auth	0008_alter_user_username_max_length	2019-02-21 07:29:24.274854+00
11	auth	0009_alter_user_last_name_max_length	2019-02-21 07:29:24.287737+00
12	users	0001_initial	2019-02-21 07:29:25.138639+00
13	admin	0001_initial	2019-02-21 07:29:25.349428+00
14	admin	0002_logentry_remove_auto_add	2019-02-21 07:29:25.377238+00
15	admin	0003_logentry_add_action_flag_choices	2019-02-21 07:29:25.391106+00
16	sessions	0001_initial	2019-02-21 07:29:25.561503+00
17	tasks	0001_initial	2019-02-21 07:29:25.894475+00
18	authtoken	0001_initial	2019-02-21 09:43:57.051755+00
19	authtoken	0002_auto_20160226_1747	2019-02-21 09:43:57.129502+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
xwkqc4imwbqmtezstvf30iqkd98r7k5x	ZGJiMjRhNGE1M2U2MmM2N2YzNTIxNzA1YTM5N2Y3ZWFhYjNiM2NjZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJhOGNkM2MzNWFhNTAzM2RjZGE2ZjQwYTI4MDk1MTU1MzljMDU2YzU1In0=	2019-03-07 10:52:25.542096+00
zdo8qoal9tq05952v1ejo1rqre1m9h3q	ZGJiMjRhNGE1M2U2MmM2N2YzNTIxNzA1YTM5N2Y3ZWFhYjNiM2NjZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJhOGNkM2MzNWFhNTAzM2RjZGE2ZjQwYTI4MDk1MTU1MzljMDU2YzU1In0=	2019-03-07 12:06:33.465684+00
u5lpngwddgodj830auyaxshdf2mz6n5c	ZGJiMjRhNGE1M2U2MmM2N2YzNTIxNzA1YTM5N2Y3ZWFhYjNiM2NjZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJhOGNkM2MzNWFhNTAzM2RjZGE2ZjQwYTI4MDk1MTU1MzljMDU2YzU1In0=	2019-03-07 12:15:24.736537+00
\.


--
-- Data for Name: tasks_task; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.tasks_task (id, name, description, completed) FROM stdin;
1	qweqwe	asdasd	f
2	qweqwe	asdasd	f
3	cccccc	asdasasd	f
4	cccccc	asdasasd	f
5	task1	description task1	f
\.


--
-- Data for Name: tasks_task_users; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.tasks_task_users (id, task_id, user_id) FROM stdin;
\.


--
-- Data for Name: users_token; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.users_token (id, token, created, user_id) FROM stdin;
1	d92cdb3e7ca3237482be0b2e57012b1c49d5eceb	2019-02-21 13:44:12.074983+00	1
2	3600e380638e79c5d79eb22b877afa013bc9578e	2019-02-21 13:57:09.953947+00	1
\.


--
-- Data for Name: users_user; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.users_user (id, password, last_login, is_superuser, is_staff, is_active, date_joined, first_name, last_name, patronymic, email) FROM stdin;
5	pbkdf2_sha256$120000$frIyuIPRAR4J$7Ruwzlb90Fqgq2ObnIajXwetkrvR0i1sz/VaM5NYmko=	\N	f	f	t	2019-02-21 12:43:15.847185+00	string	string	string	string18@ad.ad
6	pbkdf2_sha256$120000$D4x1ckcfqd7W$diZYtY8OO2t9D6cCegN+N5jZlhknt2NQKftkx3UDX6Y=	\N	f	f	t	2019-02-21 12:43:47.440821+00	string	string	string	string20@ad.ad
4	pbkdf2_sha256$120000$PoQMO50o2wXB$Ly6dYBx4VOhuqC/GkJoFJZAFjTSMz3Sf3oNgdE3mCyA=	\N	f	f	t	2019-02-21 12:28:14.215226+00	string1	string2	string3	string17@ad.ad
1	pbkdf2_sha256$120000$TYCfyRyXMMhm$76eGjs3oSf1o1JjDtn9YYrQnFimLqtLRU0Y3Pri8UVs=	2019-02-21 12:15:24.723677+00	t	t	t	2019-02-21 09:18:00.11142+00	Adminov	Admin	Adminovich	admin@admin.ad
2	pbkdf2_sha256$120000$r4rKWWYIaCbu$SFSc4fTKzB8CdCLN/lIQ9uXmptGaoMAHNPMbNFNJXss=	\N	f	f	t	2019-02-21 12:17:51.184725+00	string	string	string	string14@ad.ad
3	pbkdf2_sha256$120000$bggCFMeeQ4Zh$1mo4dhXrRgb2/OJYyWQyG2WgeVR4AGd/Ew9hdHVV/aI=	\N	f	f	t	2019-02-21 12:18:12.61359+00	string	string	string	string16@ad.ad
\.


--
-- Data for Name: users_user_groups; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.users_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: users_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: picasel_service
--

COPY public.users_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 36, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 9, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Name: tasks_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.tasks_task_id_seq', 5, true);


--
-- Name: tasks_task_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.tasks_task_users_id_seq', 1, true);


--
-- Name: users_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.users_token_id_seq', 2, true);


--
-- Name: users_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.users_user_groups_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.users_user_id_seq', 6, true);


--
-- Name: users_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: picasel_service
--

SELECT pg_catalog.setval('public.users_user_user_permissions_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: tasks_task tasks_task_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task
    ADD CONSTRAINT tasks_task_pkey PRIMARY KEY (id);


--
-- Name: tasks_task_users tasks_task_users_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task_users
    ADD CONSTRAINT tasks_task_users_pkey PRIMARY KEY (id);


--
-- Name: tasks_task_users tasks_task_users_task_id_user_id_a4bccc76_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task_users
    ADD CONSTRAINT tasks_task_users_task_id_user_id_a4bccc76_uniq UNIQUE (task_id, user_id);


--
-- Name: users_token users_token_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_pkey PRIMARY KEY (id);


--
-- Name: users_token users_token_token_key; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_token_key UNIQUE (token);


--
-- Name: users_user users_user_email_key; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_email_key UNIQUE (email);


--
-- Name: users_user_groups users_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_pkey PRIMARY KEY (id);


--
-- Name: users_user_groups users_user_groups_user_id_group_id_b88eab82_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_group_id_b88eab82_uniq UNIQUE (user_id, group_id);


--
-- Name: users_user users_user_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user
    ADD CONSTRAINT users_user_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions users_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: users_user_user_permissions users_user_user_permissions_user_id_permission_id_43338c45_uniq; Type: CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_permission_id_43338c45_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: tasks_task_name_c493070a; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX tasks_task_name_c493070a ON public.tasks_task USING btree (name);


--
-- Name: tasks_task_name_c493070a_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX tasks_task_name_c493070a_like ON public.tasks_task USING btree (name varchar_pattern_ops);


--
-- Name: tasks_task_users_task_id_6f70115d; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX tasks_task_users_task_id_6f70115d ON public.tasks_task_users USING btree (task_id);


--
-- Name: tasks_task_users_user_id_8d2b23cf; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX tasks_task_users_user_id_8d2b23cf ON public.tasks_task_users USING btree (user_id);


--
-- Name: users_token_token_8d431dd8_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_token_token_8d431dd8_like ON public.users_token USING btree (token varchar_pattern_ops);


--
-- Name: users_token_user_id_af964690; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_token_user_id_af964690 ON public.users_token USING btree (user_id);


--
-- Name: users_user_email_243f6e77_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_email_243f6e77_like ON public.users_user USING btree (email varchar_pattern_ops);


--
-- Name: users_user_first_name_1bd0e2c1; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_first_name_1bd0e2c1 ON public.users_user USING btree (first_name);


--
-- Name: users_user_first_name_1bd0e2c1_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_first_name_1bd0e2c1_like ON public.users_user USING btree (first_name varchar_pattern_ops);


--
-- Name: users_user_groups_group_id_9afc8d0e; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_groups_group_id_9afc8d0e ON public.users_user_groups USING btree (group_id);


--
-- Name: users_user_groups_user_id_5f6f5a90; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_groups_user_id_5f6f5a90 ON public.users_user_groups USING btree (user_id);


--
-- Name: users_user_last_name_fb97206b; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_last_name_fb97206b ON public.users_user USING btree (last_name);


--
-- Name: users_user_last_name_fb97206b_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_last_name_fb97206b_like ON public.users_user USING btree (last_name varchar_pattern_ops);


--
-- Name: users_user_patronymic_093ce864; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_patronymic_093ce864 ON public.users_user USING btree (patronymic);


--
-- Name: users_user_patronymic_093ce864_like; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_patronymic_093ce864_like ON public.users_user USING btree (patronymic varchar_pattern_ops);


--
-- Name: users_user_user_permissions_permission_id_0b93982e; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_user_permissions_permission_id_0b93982e ON public.users_user_user_permissions USING btree (permission_id);


--
-- Name: users_user_user_permissions_user_id_20aca447; Type: INDEX; Schema: public; Owner: picasel_service
--

CREATE INDEX users_user_user_permissions_user_id_20aca447 ON public.users_user_user_permissions USING btree (user_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tasks_task_users tasks_task_users_task_id_6f70115d_fk_tasks_task_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task_users
    ADD CONSTRAINT tasks_task_users_task_id_6f70115d_fk_tasks_task_id FOREIGN KEY (task_id) REFERENCES public.tasks_task(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tasks_task_users tasks_task_users_user_id_8d2b23cf_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.tasks_task_users
    ADD CONSTRAINT tasks_task_users_user_id_8d2b23cf_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_token users_token_user_id_af964690_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_token
    ADD CONSTRAINT users_token_user_id_af964690_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups users_user_groups_group_id_9afc8d0e_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_group_id_9afc8d0e_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_groups users_user_groups_user_id_5f6f5a90_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_groups
    ADD CONSTRAINT users_user_groups_user_id_5f6f5a90_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_permissions users_user_user_perm_permission_id_0b93982e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_perm_permission_id_0b93982e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_user_user_permissions users_user_user_permissions_user_id_20aca447_fk_users_user_id; Type: FK CONSTRAINT; Schema: public; Owner: picasel_service
--

ALTER TABLE ONLY public.users_user_user_permissions
    ADD CONSTRAINT users_user_user_permissions_user_id_20aca447_fk_users_user_id FOREIGN KEY (user_id) REFERENCES public.users_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

